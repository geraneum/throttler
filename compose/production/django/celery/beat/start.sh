#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


celery -A throttler.celery beat -l INFO
