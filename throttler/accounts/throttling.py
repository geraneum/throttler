import random
from datetime import timedelta

from django.core.cache import cache
from django.utils import timezone

from rest_framework import throttling


class LoginRateThrottle(throttling.BaseThrottle):
    SUBJECT_FAILED_ATTEMPTS_LIMIT = 3
    ADDRESS_FAILED_ATTEMPTS_LIMIT = 100

    FAILED_ATTEMPTS_GRACE = timedelta(seconds=60)

    def get_subject_cache_key(self, request):
        stored_credentials = cache.get(request.data.get('nonce', None), None)
        return 'SUBJECT_FAILED_LOGINS_{}'.format(stored_credentials.get('subject', None)) if stored_credentials else None

    def get_address_cache_key(self, request):
        return 'ADDRESS_FAILED_LOGINS_{}'.format(self.get_ident(request))

    def allow_request(self, request, view):
        # get previous failed attempts from cache
        subject_failed_logins = cache.get(self.get_subject_cache_key(request), None)

        # if previous attempts are less that allowed limit, do not throttle!
        if subject_failed_logins is not None and subject_failed_logins['attempts'] >= self.SUBJECT_FAILED_ATTEMPTS_LIMIT and timezone.now() < subject_failed_logins['updated'] + self.FAILED_ATTEMPTS_GRACE:
            return False

        # get previous failed attempts from cache
        address_failed_logins = cache.get(self.get_address_cache_key(request), None)

        # if previous attempts are less that allowed limit, do not throttle!
        return address_failed_logins is None or address_failed_logins['attempts'] < self.ADDRESS_FAILED_ATTEMPTS_LIMIT and timezone.now() > subject_failed_logins['updated'] + self.FAILED_ATTEMPTS_GRACE
