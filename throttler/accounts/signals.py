from django.db.models.signals import post_save
from django.dispatch import receiver, Signal
from django.core.cache import cache
from django.utils import timezone

from rest_framework.request import Request

from .models import User, Profile
from .throttling import LoginRateThrottle
from .serializers import VerifyCallbackSerializer, verification_failed


@receiver(post_save, sender=User, dispatch_uid='accounts.create_user_profile')
def create_user_profile(sender, instance: User, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(verification_failed, sender=VerifyCallbackSerializer, dispatch_uid='accounts.log_verification_failure')
def log_verification_failure(sender, request: Request, subject, **kwargs):
    # set the failure counter for subject
    subject_cache_key = LoginRateThrottle().get_subject_cache_key(request)
    previous_subject_failures = cache.get_or_set(subject_cache_key, {'attempts': 0})
    previous_subject_failures['attempts'] = previous_subject_failures['attempts'] + 1
    previous_subject_failures['updated'] = timezone.now()
    cache.set(subject_cache_key, previous_subject_failures)
    # set the failure counter for address
    address_cache_key = LoginRateThrottle().get_address_cache_key(request)
    previous_address_failures = cache.get_or_set(address_cache_key, {'attempts': 0})
    previous_address_failures['attempts'] = previous_subject_failures['attempts'] + 1
    previous_address_failures['updated'] = timezone.now()
    cache.set(address_cache_key, previous_address_failures)
