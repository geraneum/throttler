from django.conf.urls import url
from rest_framework import routers

from .views import UserView, ProfileView, LoginView, EmailView, LogoutView, SessionView

router = routers.DefaultRouter()
router.include_root_view = False

router.register('sessions', SessionView, base_name='sessions')

urlpatterns = router.urls + [
    url(r'login/', LoginView.as_view(), name='login'),
    url(r'logout/', LogoutView.as_view(), name='logout'),
    url(r'user/', UserView.as_view(), name='user'),
    url(r'email/', EmailView.as_view(), name='email'),
    url(r'profile/', ProfileView.as_view(), name='profile'),
]
