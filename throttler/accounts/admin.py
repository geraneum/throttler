from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import ugettext_lazy as _

from .models import AuthToken, User, Profile


class ThrottlerUserCreationForm(UserCreationForm):
    """
    A form that creates a user, with no password, from the given email.
    """
    class Meta:
        model = User
        fields = ['phone', 'email']


class ProfileInlineAdmin(admin.StackedInline):
    model = Profile
    fields = ['created', 'updated']
    readonly_fields = ['created', 'updated']
    classes = ['grp-collapse grp-open']
    inline_classes = ['grp-collapse grp-open']


class AuthTokenAdmin(admin.StackedInline):
    model = AuthToken
    fields = ['pk', 'digest', 'key', 'salt', 'user', 'expires']
    readonly_fields = fields
    classes = ['grp-collapse grp-open']
    inline_classes = ['grp-collapse grp-open']
    max_num = 0


@admin.register(User)
class ThrottlerUserAdmin(UserAdmin):
    add_form = ThrottlerUserCreationForm
    fieldsets = [
        (None, {'fields': ['id', 'phone', 'email', 'password']}),
        (_('personal info'), {'fields': ['first_name', 'last_name']}),
        (_('permissions'), {'fields': ['is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions']}),
        (_('important dates'), {'fields': ['last_login', 'date_joined']}),
    ]
    add_fieldsets = [
        (None, {
            'classes': ('wide',),
            'fields': ['phone', 'email', 'first_name', 'last_name', 'password1', 'password2'],
        }),
    ]
    readonly_fields = ['id', 'last_login', 'date_joined']
    list_display = ['national_formatted_phone', 'full_name', 'email', 'date_joined']
    list_filter = ['is_staff', 'is_superuser', 'is_active', 'date_joined', 'groups']
    search_fields = ['id', 'phone', 'first_name', 'last_name', 'email']
    ordering = ['-date_joined']
    filter_horizontal = ['groups', 'user_permissions']
    inlines = [ProfileInlineAdmin, AuthTokenAdmin]

    def full_name(self, obj: User):
        return obj.get_full_name()

    def national_formatted_phone(self, obj: User):
        return str(obj)
