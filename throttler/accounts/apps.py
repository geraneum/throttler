from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'throttler.accounts'
    verbose_name = "Accounts"

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        try:
            import throttler.accounts.signals  # noqa F401
        except ImportError:
            pass
