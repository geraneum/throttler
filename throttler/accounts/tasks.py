from __future__ import absolute_import, unicode_literals

from django.conf import settings
from django.core.mail import send_mail

import requests
from celery import shared_task


@shared_task(name='users.send_verification_sms', default_retry_delay=30, max_retries=4)
def send_verification_sms(phone: str, verification_code: str) -> None:
    """
    Send the verification code via an sms service provider.
    This function expects `phone` to be in E.164 format, See: https://en.wikipedia.org/wiki/E.164

    :param phone: str
    :param verification_code: str
    :return: Boolean
    """
    if not settings.DEBUG:
        requests.get(
            params={
                'receptor': '0' + phone[2:],
                'token': verification_code,
                'template': 'verify',
            },
            url=settings.KAVEH_NEGAR_SMS_SERVICE['SEND_URL'],
            timeout=10,
        )
    else:
        print('Celery Worker: Verification code for {} is {}'.format(phone, verification_code))


@shared_task(name='users.send_verification_email', default_retry_delay=30, max_retries=4)
def send_verification_email(email: str, verification_code: str) -> None:
    """
    Send the verification code via an email service provider.

    :param email: str
    :param verification_code: str
    :return: Boolean
    """
    if not settings.DEBUG:
        send_mail(
            'Throttler Email Verification Code',
            'Your Throttler email verification code is {}'.format(verification_code),
            'noreply@throttler.com',
            [email],
            fail_silently=False,
        )
    else:
        print('Celery Worker: Verification code for {} is {}'.format(email, verification_code))
