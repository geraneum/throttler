import secrets

from django.contrib.auth import user_logged_in, user_logged_out

from rest_framework import viewsets, views, mixins, generics, status, exceptions, permissions
from rest_framework.permissions import AllowAny, IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.decorators import throttle_classes
from rest_framework.response import Response

from .models import User, AuthToken
from .tasks import send_verification_sms, send_verification_email
from .throttling import LoginRateThrottle
from .serializers import (
    UserSerializer,
    ProfileSerializer,
    SessionSerializer,
    RegisterPhoneSerializer,
    UpdateEmailSerializer,
    VerifyCallbackSerializer
)

secret_random = secrets.SystemRandom()


class SessionsPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        return request.method in permissions.SAFE_METHODS

    def has_object_permission(self, request, view, obj):
        # for the user that this object belongs to,
        # he/she should be able to view or delete it
        return obj.user == request.user and request.method in permissions.SAFE_METHODS + ('DELETE',)


class UserView(generics.RetrieveUpdateAPIView):
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = (DjangoFilterBackend,)

    def get_object(self):
        return self.request.user


class ProfileView(generics.RetrieveUpdateAPIView):
    serializer_class = ProfileSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = (DjangoFilterBackend,)

    def get_object(self):
        return self.request.user.profile


class SessionView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, SessionsPermission)
    serializer_class = SessionSerializer

    def get_queryset(self):
        return AuthToken.objects.filter(user=self.request.user)

    def update(self, request, *args, **kwargs):
        raise exceptions.MethodNotAllowed

    def partial_update(self, request, *args, **kwargs):
        raise exceptions.MethodNotAllowed

    def create(self, request, *args, **kwargs):
        raise exceptions.MethodNotAllowed


class LoginView(mixins.CreateModelMixin, mixins.UpdateModelMixin, generics.GenericAPIView):
    """
    This view is used to initiate and validate user provided `nonce` and `code`, then generate token.
    After validating credentials, if a user object does not exist for provided phone,
    a user object and a token will be created, else, just a token would be created
    and the result is returned. Response's status code for newly created users would be 201
    and for returning users it would be 202.
    """
    permission_classes = (AllowAny,)
    queryset = AuthToken.objects.none()

    def _send_verification_sms(self, phone, code):
        send_verification_sms.delay(phone, code)

    def _login_user(self, user: User):
        AuthToken.objects.filter(user=user).delete()  # for now only one session is allowed
        token = AuthToken.objects.create(user=user)
        user_logged_in.send(sender=user.__class__, request=self.request, user=user)
        return str(token)

    def _create_or_update_user(self, phone):
        user, created = User.objects.get_or_create(phone=phone)
        if not user.is_staff and not user.is_superuser:
            user.set_unusable_password()
        token = self._login_user(user)
        return user, created, token

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return RegisterPhoneSerializer
        if self.request.method == 'PUT':
            return VerifyCallbackSerializer

    def update(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        user, created, token = self._create_or_update_user(serializer.data['subject'])
        headers = self.get_success_headers(serializer.data)
        return Response(
            data={'token': token},
            status=status.HTTP_201_CREATED if created else status.HTTP_202_ACCEPTED,
            headers=headers
        )

    def perform_update(self, serializer):
        serializer.save()

    @throttle_classes([LoginRateThrottle])
    def put(self, request, *args, **kwargs):
        return self.update(request, args, kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            data={'nonce': serializer.data['nonce']},
            status=status.HTTP_201_CREATED,
            headers=headers
        )

    def perform_create(self, serializer):
        serializer.save()
        self._send_verification_sms(serializer.data['phone'], serializer.data['code'])

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class EmailView(mixins.CreateModelMixin, mixins.UpdateModelMixin, generics.GenericAPIView):
    """
    This view is used to validate the email address of user using provided `nonce` and `code`.
    """
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.none()

    def _send_verification_email(self, email, code):
        send_verification_email.delay(email, code)

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return UpdateEmailSerializer
        if self.request.method == 'PUT':
            return VerifyCallbackSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(data={'nonce': serializer.data['nonce']}, status=status.HTTP_200_OK)

    def perform_create(self, serializer):
        serializer.save()
        self._send_verification_email(serializer.data['email'], serializer.data['code'])

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(data={}, status=status.HTTP_202_ACCEPTED, headers=headers)

    def perform_update(self, serializer):
        self.request.user.email = serializer.data['subject']
        self.request.user.save()

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)


class LogoutView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def _logout_user(self):
        user = self.request.user
        AuthToken.objects.filter(user=user).delete()  # delete all tokens!
        user_logged_out.send(sender=user.__class__, request=self.request, user=user)

    def post(self, *args, **kwargs):
        # logout user
        self._logout_user()
        # return an appropriate response
        return Response(data={}, status=status.HTTP_200_OK)
