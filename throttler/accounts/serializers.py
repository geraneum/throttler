import secrets

from django.core.cache import cache
from django.dispatch import Signal
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from rest_framework.authentication import get_authorization_header

from .models import AuthToken, User, Profile, phone_validator

secret_random = secrets.SystemRandom()

verification_failed = Signal(providing_args=['request', 'subject'])


class InitializeVerificationSerializer(serializers.Serializer):
    nonce = serializers.SerializerMethodField()
    code = serializers.SerializerMethodField()

    class Meta:
        verification_fields = None
        verification_code_length = 6
        verification_nonce_length = 27
        verification_nonce_ttl = 60 * 5

    def get_nonce(self, obj):
        return secrets.token_urlsafe(self.Meta.verification_nonce_length)

    def get_code(self, obj):
        return str(
            secret_random.sample(
                range(10 ** (self.Meta.verification_code_length - 1), 10 ** self.Meta.verification_code_length), 1
            )[0]
        )

    def create(self, validated_data):
        # for each requested verification field
        for verification_field in self.Meta.verification_fields:
            # prepare verification data
            verification_data = {
                'type': verification_field,
                'subject': self.validated_data[verification_field],
                'code': self.data['code'],
            }
            # cache verification data
            cache.set(self.data['nonce'], verification_data, self.Meta.verification_nonce_ttl)
        # return verification data
        return self.data


class VerifyCallbackSerializer(serializers.Serializer):
    nonce = serializers.CharField(required=True)
    code = serializers.CharField(required=True)
    subject = serializers.SerializerMethodField()

    def validate_code(self, value):
        # get cached verification data with nonce as key
        stored_credentials = cache.get(self.initial_data['nonce'], None)
        # check and verify data and code
        if stored_credentials is None or len(value) == 0 or value != stored_credentials['code']:
            verification_failed.send(sender=self.__class__, request=self.context['request'], subject=stored_credentials['subject'])
            raise serializers.ValidationError(_('this code is not valid'))

    def get_subject(self, obj):
        # get cached verification data with nonce as key
        stored_credentials = cache.get(self.validated_data['nonce'])
        # return subject value
        return stored_credentials['subject']

    def create(self, validated_data):
        # return verification data along with subject
        return self.data


class RegisterPhoneSerializer(InitializeVerificationSerializer):
    phone = serializers.CharField(validators=[phone_validator], max_length=15, required=True)

    class Meta(InitializeVerificationSerializer.Meta):
        verification_fields = ['phone']


class UpdateEmailSerializer(InitializeVerificationSerializer):
    email = serializers.EmailField(required=True)

    class Meta(InitializeVerificationSerializer.Meta):
        verification_fields = ['email']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'phone', 'first_name', 'last_name', 'email']
        read_only_fields = ['id', 'phone', 'email']


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = []


class SessionSerializer(serializers.ModelSerializer):
    current = serializers.SerializerMethodField()

    class Meta:
        model = AuthToken
        fields = ['id', 'created', 'expires', 'current']

    def get_current(self, obj: AuthToken):
        auth_header = get_authorization_header(self.context['request']).split()
        if not auth_header:
            return False
        return obj.key == AuthToken.objects.get_key(auth_header[1]).decode()
