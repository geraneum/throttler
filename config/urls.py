from collections import OrderedDict

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import NoReverseMatch, path
from django.views import defaults as default_views
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView

from rest_framework.response import Response
from rest_framework.reverse import reverse, reverse_lazy
from rest_framework.routers import APIRootView


class ThrottlerAPIRootView(APIRootView):
    """
    This is a url mapping of Throttler v1 api.
    Purpose of this view is to help consumers get a better idea about this version of the api.
    """
    _ignore_model_permissions = True
    schema = None  # exclude from schema
    api_root_dict = {
        'auth': {
            'login': 'v1:auth:login',
            'logout': 'v1:auth:logout',
            'user': 'v1:auth:user',
            'email': 'v1:auth:email',
            'profile': 'v1:auth:profile',
            'sessions': 'v1:auth:sessions-list',
        },
    }

    def get(self, request, *args, **kwargs):
        # Return a plain {"name": "hyperlink"} response.
        ret = OrderedDict()
        for namespace, urls in self.api_root_dict.items():
            ret[namespace] = OrderedDict()
            for key, url_name in urls.items():
                try:
                    ret[namespace][key] = reverse(
                        url_name,
                        args=args,
                        kwargs=kwargs,
                        request=request,
                        format=kwargs.get('format', None)
                    )
                except NoReverseMatch:
                    print(url_name)
                    # Don't bail out if eg. no list routes exist, only detail routes.
                    continue
        # return the final urls dict
        return Response(ret)

urlpatterns = [
    # Django Grappelli
    path('grappelli/', include('grappelli.urls')),
    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, admin.site.urls),

    # API Root View Session Auth
    url(r'^root-view-auth/', include('rest_framework.urls', namespace='rest_framework')),

    # Throttler API V1
    url(r'^v1/', include(([

        url(r'auth/', include(('throttler.accounts.urls', 'accounts'), namespace='auth')),

        # === API Root View ===
        url(r'^$', ThrottlerAPIRootView.as_view(), name='root'),

    ], 'throttler-api-v1'), namespace='v1')),

    # Service Root View
    url(r'^$', RedirectView.as_view(url=reverse_lazy('v1:root'), permanent=False)),

    # Your stuff: custom urls includes go here
] + static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(
            r"^400/$",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        url(
            r"^403/$",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        url(
            r"^404/$",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        url(r"^500/$", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [url(r"^__debug__/", include(debug_toolbar.urls))] + urlpatterns
